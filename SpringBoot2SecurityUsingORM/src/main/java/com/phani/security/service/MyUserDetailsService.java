package com.phani.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.phani.security.bean.MyUserDetails;
import com.phani.security.bean.User;
import com.phani.security.repo.UserRepository;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userRepository.findByUname(username);
		if(user == null) {
			throw new UsernameNotFoundException("User NOT Found!");
		}
		return new MyUserDetails(user);
	}
	
	
}

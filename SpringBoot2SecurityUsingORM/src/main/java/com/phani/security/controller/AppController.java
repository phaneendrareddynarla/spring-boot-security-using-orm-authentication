package com.phani.security.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {
	
	@GetMapping("/home")
	public String showHome() {
		return "HomePage";
	}
	
	@GetMapping("/emp")
	public String showEmployee() {
		return "EmployeePage";
	}
	
	@GetMapping("/admin")
	public String showAdmin() {
		return "AdminPage";
	}
	
	@GetMapping("/student")
	public String showStudent() {
		return "StudentPage";
	}
	
	@GetMapping("/profile")
	public String showProfile() {
		return "ProfilePage";
	}
	
	@GetMapping("/denied")
	public String showDenied() {
		return "AccessDeniedPage";
	}
}
